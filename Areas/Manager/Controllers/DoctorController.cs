using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Davinci.Data.Models;
using Davinci.Data.Repositories;
using Davinci.Models.Dto;
using Davinci.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Piranha;
using Piranha.Manager;

namespace Davinci.Areas.Manager.Controllers
{
    [Route("manager/api/doctor")]
    [Authorize(Policy = Permission.Admin)]
    public class DoctorController : ControllerBase
    {
        private readonly DoctorRepository _repository;

        private readonly DoctorService _service;

        private readonly IApi _api;

        public DoctorController(DoctorRepository repository, DoctorService service, IApi api)
        {
            _repository = repository;
            _service = service;
            _api = api;
        }

        [Route("{id:Guid}")]
        [HttpGet]
        public async Task<ActionResult> GetById(Guid id)
        {
            var doctor = await _repository.GetById(id);

            if (doctor == null)
            {
                return NotFound();
            }

            var photo = await _api.Media.GetByIdAsync(doctor.Photo);
            var page = await _api.Pages.GetByIdAsync(doctor.PageLink);

            return Ok(new DoctorModel
            {
                Id = doctor.Id,
                Name = doctor.Name,
                About = doctor.About,
                PageLink = doctor.PageLink,
                PageLinkUrl = "/" + page?.Slug,
                Photo = doctor.Photo,
                PhotoUrl = photo?.PublicUrl.Replace("~", "")
            });
        }

        [Route("list")]
        [HttpGet]
        public async Task<List<DoctorModel>> GetList()
        {
            var list = await _repository.GetList();

            var result = new List<DoctorModel>();

            foreach (var doctor in list)
            {
                var photo = await _api.Media.GetByIdAsync(doctor.Photo);
                var page = await _api.Pages.GetByIdAsync(doctor.PageLink);

                result.Add(new DoctorModel
                {
                    Id = doctor.Id,
                    Name = doctor.Name,
                    About = doctor.About,
                    PageLink = doctor.PageLink,
                    PageLinkUrl = "/" + page?.Slug,
                    Photo = doctor.Photo,
                    PhotoUrl = photo?.PublicUrl.Replace("~", "")
                });
            }

            return result;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ActionResult> Save([FromBody] DoctorModel dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var doctor = new Doctor
            {
                Id = dto.Id ?? Guid.Empty,
                Name = dto.Name,
                About = dto.About,
                PageLink = dto.PageLink,
                Photo = dto.Photo
            };

            await _service.Save(doctor);

            return Ok();
        }

        [Route("delete/{id:Guid}")]
        [HttpPost]
        public async Task<ActionResult> Delete(Guid id)
        {
            var doctor = await _repository.GetById(id);
            if (doctor != null)
            {
                await _service.Delete(doctor);
            }

            return Ok();
        }
    }
}