using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Data.Models;
using Davinci.Data.Repositories;
using Davinci.Models.Dto;
using Davinci.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Piranha.Manager;

namespace Davinci.Areas.Manager.Controllers
{
    [ApiController, Area("Manager")]
    [Route("manager/api/product")]
    [Authorize(Policy = Permission.Admin)]
    public class ProductController : ControllerBase
    {
        private readonly ProductRepository _productRepository;

        private readonly CategoryRepository _categoryRepository;

        private readonly ProductService _productService;

        public ProductController(ProductRepository productRepository, CategoryRepository categoryRepository,
            ProductService productService)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _productService = productService;
        }

        [Route("{id:Guid}")]
        [HttpGet]
        public async Task<ActionResult> GetById(Guid id)
        {
            var product = await _productRepository.GetById(id);

            if (product == null)
            {
                return NotFound();
            }

            var categories = product.ProductCategories
                .Select(m => new ProductModel.CategoryItem(m.Category.Id, m.Category.Title))
                .ToList();

            return Ok(new ProductModel
            {
                Id = product.Id,
                Title = product.Title,
                Description = product.Description,
                Price = product.Price,
                MinimumPrice = product.MinimumPrice,
                MaximumPrice = product.MaximumPrice,
                CreatedAt = product.CreatedAt,
                UpdatedAt = product.UpdatedAt,
                Categories = categories
            });
        }

        [Route("list")]
        [HttpGet]
        public async Task<List<ProductModel>> GetList(Guid? categoryId)
        {
            var list = await _productRepository.GetList(categoryId);
            return list.Select(m => new ProductModel
            {
                Id = m.Id,
                Title = m.Title,
                Description = m.Description,
                Price = m.Price,
                MinimumPrice = m.MinimumPrice,
                MaximumPrice = m.MaximumPrice,
                CreatedAt = m.CreatedAt,
                UpdatedAt = m.UpdatedAt
            }).ToList();
        }

        [Route("save")]
        [HttpPost]
        public async Task<ActionResult> Save(ProductModel dto)
        {
            var product = new Product(dto.Title, dto.Description)
            {
                Id = dto.Id ?? Guid.Empty,
                Price = dto.Price,
                MinimumPrice = dto.MinimumPrice,
                MaximumPrice = dto.MaximumPrice
            };

            if (dto.Categories.Any())
            {
                var guids = dto.Categories.Select(m => m.Id).ToList();
                var categories = await _categoryRepository.GetList(guids, new Options {NoTracking = false});

                product.ProductCategories = categories
                    .Select(m => new ProductCategory(product, m))
                    .ToList();
            }

            await _productService.Save(product);

            return Ok();
        }

        [Route("delete/{id:Guid}")]
        [HttpPost]
        public async Task<ActionResult> Delete(Guid id)
        {
            var product = await _productRepository.GetById(id);
            if (product != null)
            {
                await _productService.Delete(product);
            }

            return Ok();
        }
    }
}