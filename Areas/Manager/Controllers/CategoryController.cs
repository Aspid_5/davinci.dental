using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Data.Models;
using Davinci.Data.Repositories;
using Davinci.Models.Dto;
using Davinci.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Piranha.Manager;

namespace Davinci.Areas.Manager.Controllers
{
    [ApiController, Area("Manager")]
    [Route("manager/api/category")]
    [Authorize(Policy = Permission.Admin)]
    public class CategoryController : ControllerBase
    {
        private readonly CategoryRepository _repository;

        private readonly CategoryService _service;

        public CategoryController(CategoryRepository repository, CategoryService service)
        {
            _repository = repository;
            _service = service;
        }

        [Route("{id:Guid}")]
        [HttpGet]
        public async Task<ActionResult> Get(Guid id)
        {
            var category = await _repository.GetById(id);
            if (category == null)
            {
                return NotFound();
            }

            return Ok(new CategoryModel
            {
                Id = category.Id,
                Title = category.Title,
                ParentId = category.ParentId,
                SortOrder = category.SortOrder
            });
        }

        [Route("list")]
        [HttpGet]
        public async Task<List<CategoryMapModel>> List()
        {
            var list = await _repository.GetList();

            var rootNodes = list.Where(m => m.ParentId == null).ToList();
            var result = new List<CategoryMapModel>();

            foreach (var category in rootNodes)
            {
                result.Add(CategoryMapModel.BuildMap(category, list));
            }

            return result;
        }

        [Route("save")]
        [HttpPost]
        public async Task<ActionResult> Save(CategoryModel dto)
        {
            var category = new Category
            {
                Id = dto.Id ?? Guid.Empty,
                Title = dto.Title,
                ParentId = dto.ParentId,
                SortOrder = dto.SortOrder
            };

            await _service.Save(category);

            return Ok();
        }

        [Route("move")]
        [HttpPost]
        public async Task<ActionResult> Move(StructureModel structure)
        {
            var position = FindPosition(structure.Id, structure.Items);
            if (position == null)
            {
                return NotFound();
            }

            var category = await _repository.GetById(structure.Id);
            if (category == null)
            {
                return NotFound();
            }

            var parent = position.Item1.HasValue 
                ? await _repository.GetById(position.Item1.Value) 
                : null;

            await _service.Move(category, parent, position.Item2);

            return Ok();
        }

        [Route("delete/{id:Guid}")]
        [HttpPost]
        public async Task<ActionResult> Delete(Guid id)
        {
            var category = await _repository.GetById(id);
            if (category != null)
            {
                await _service.Delete(category);
            }

            return Ok();
        }

        /// <summary>
        /// Поиск позиции needleId в списке items
        /// </summary>
        /// <param name="needleId">Искомый id</param>
        /// <param name="items">Список элементов</param>
        /// <param name="parentId">Родитель</param>
        /// <returns>Кортеж с родительским id и позицей</returns>
        private static Tuple<Guid?, int> FindPosition(Guid needleId, IList<StructureModel.Item> items,
            Guid? parentId = null)
        {
            for (var index = 0; index < items.Count; index++)
            {
                if (needleId == items[index].Id)
                {
                    return new Tuple<Guid?, int>(parentId, index);
                }

                if (items[index].Children.Count > 0)
                {
                    var position = FindPosition(needleId, items[index].Children, items[index].Id);
                    if (position != null)
                    {
                        return position;
                    }
                }
            }

            return null;
        }
    }
}