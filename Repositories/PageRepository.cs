using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Models;
using Microsoft.EntityFrameworkCore;
using Piranha;
using Piranha.Data;

namespace Davinci.Repositories
{
    public class PageRepository
    {
        private readonly IDb _db;

        public PageRepository(IDb db)
        {
            _db = db;
        }

        public async Task<IList<Page>> GetMedicalAidPages()
        {
            return await _db.Pages
                .AsNoTracking()
                .Where(p => p.PageTypeId == nameof(ServicePage))
                .Where(p => p.Published.HasValue && p.Published < DateTime.Now)
                .OrderBy(p => p.SortOrder)
                .ToListAsync();
        }
    }
}