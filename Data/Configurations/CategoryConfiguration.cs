using Davinci.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Davinci.Data.Configurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.Property(m => m.Title)
                .IsRequired()
                .HasMaxLength(512);

            builder.HasIndex(m => new {m.ParentId, m.SortOrder})
                .IsUnique();
        }
    }
}