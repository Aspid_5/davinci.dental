using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Davinci.Data.Repositories
{
    public class ProductRepository
    {
        private readonly Db _db;

        public ProductRepository(Db db)
        {
            _db = db;
        }

        public async Task<Product> GetById(Guid id)
        {
            return await _db.Products
                .Include(m => m.ProductCategories)
                    .ThenInclude(m => m.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
        }

        /// <summary>
        /// Получение товаров по фильтру
        /// </summary>
        /// <param name="categoryId">Товары из этой категории, null = все категории</param>
        /// <param name="opt"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetList(Guid? categoryId = null, Options opt = null)
        {
            var options = opt ?? Options.GetDefault();
            
            var query= _db.Products.AsQueryable();

            if (options.NoTracking)
            {
                query = query.AsNoTracking();
            }

            if (options.FullEntry)
            {
                query = query
                    .Include(m => m.ProductCategories)
                    .ThenInclude(m => m.Category);
            }

            if (categoryId.HasValue)
            {
                query = query
                    .Join(_db.ProductCategories,
                        product => product.Id,
                        productCategory => productCategory.ProductId,
                        (product, productCategory) => new {Product = product, ProductCategory = productCategory}
                    )
                    .Where(pair => pair.ProductCategory.CategoryId == categoryId)
                    .Select(pair => pair.Product);
            }

            return await query
                .OrderBy(m => m.CreatedAt)
                .ToListAsync();
        }
    }
}