namespace Davinci.Data.Repositories
{
    public class Options
    {
        public bool NoTracking { get; set; }
        
        public bool FullEntry { get; set; }

        public Options()
        {
            NoTracking = true;
            FullEntry = false;
        }

        public static Options GetDefault()
        {
            return new Options();
        }
    }
}