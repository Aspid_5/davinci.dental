using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Davinci.Data.Repositories
{
    public class DoctorRepository
    {
        private readonly Db _db;

        public DoctorRepository(Db db)
        {
            _db = db;
        }

        public async Task<Doctor> GetById(Guid id)
        {
            return await _db.Doctors.FirstOrDefaultAsync(m => m.Id == id);
        }

        /// <summary>
        /// Получить список врачей
        /// </summary>
        /// <param name="opt"></param>
        /// <returns></returns>
        public async Task<List<Doctor>> GetList(Options opt = null)
        {
            var options = opt ?? Options.GetDefault();

            var query = _db.Doctors.AsQueryable();

            if (options.NoTracking)
            {
                query = query.AsNoTracking();
            }

            return await query
                .OrderBy(m => m.Id)
                .ToListAsync();
        }
    }
}