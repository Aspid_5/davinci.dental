using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Davinci.Data.Repositories
{
    public class CategoryRepository
    {
        private readonly Db _db;

        public CategoryRepository(Db db)
        {
            _db = db;
        }
        
        public async Task<Category> GetById(Guid id)
        {
            return await _db.Categories.FirstOrDefaultAsync(m => m.Id == id);
        }
        
        public async Task<List<Category>> GetList(Options opt = null)
        {
            var options = opt ?? Options.GetDefault();

            var query = _db.Categories.AsQueryable();

            if (options.NoTracking)
            {
                query = query.AsNoTracking();
            }

            return await query
                .OrderBy(m => m.ParentId)
                .ThenBy(m => m.SortOrder)
                .ToListAsync();
        }

        public async Task<List<Category>> GetList(List<Guid> ids, Options opt)
        {
            var options = opt ?? Options.GetDefault();

            var query= _db.Categories.AsQueryable();

            if (options.NoTracking)
            {
                query = query.AsNoTracking();
            }

            return await query
                .Where(m => ids.Contains(m.Id))
                .OrderBy(m => m.ParentId)
                .ThenBy(m => m.SortOrder)
                .ToListAsync();
        }
    }
}