using System;

namespace Davinci.Data.Models
{
    public class ProductCategory
    {
        public Guid ProductId { get; set; }

        public Product Product { get; set; }

        public Guid CategoryId { get; set; }

        public Category Category { get; set; }

        public ProductCategory()
        {
        }

        public ProductCategory(Product product, Category category)
        {
            Product = product;
            Category = category;
        }
    }
}