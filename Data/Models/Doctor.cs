using System;

namespace Davinci.Data.Models
{
    public class Doctor
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string About { get; set; }

        public Guid PageLink { get; set; }

        public Guid Photo { get; set; }
    }
}