using System;
using System.Collections.Generic;

namespace Davinci.Data.Models
{
    public class Product
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }

        public decimal? MinimumPrice { get; set; }

        public decimal? MaximumPrice { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
        
        public ICollection<ProductCategory> ProductCategories { get; set; }

        public Product()
        {
            CreatedAt = DateTime.Now;
            ProductCategories = new List<ProductCategory>();
        }

        public Product(string title, string description) : this()
        {
            Title = title;
            Description = description;
        }
    }
}