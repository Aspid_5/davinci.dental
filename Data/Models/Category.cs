using System;
using System.Collections.Generic;

namespace Davinci.Data.Models
{
    public class Category
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public Guid? ParentId { get; set; }

        public int SortOrder { get; set; }
        
        public ICollection<ProductCategory> ProductCategories { get; set; }

        public Category()
        {
            ProductCategories = new List<ProductCategory>();
        }
    }
}