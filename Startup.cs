﻿using Davinci.Data;
using Davinci.Data.Repositories;
using Davinci.Extensions.Blocks;
using Davinci.Repositories;
using Davinci.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Piranha;
using Piranha.AspNetCore.Identity.PostgreSQL;
using Piranha.AttributeBuilder;
using Piranha.Data.EF.PostgreSql;
using Piranha.Manager;
using Piranha.Manager.Editor;
using Serilog;

namespace Davinci
{
    public class Startup
    {
        /// <summary>
        /// The application config.
        /// </summary>
        public IConfiguration Configuration { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="configuration">The current configuration</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            // Service setup
            services.AddPiranha(options =>
            {
                options.UseFileStorage();
                options.UseImageSharp();
                options.UseManager();
                options.UseTinyMCE();
                options.UseMemoryCache();
                options.UseEF<PostgreSqlDb>(db =>
                    db.UseNpgsql(Configuration.GetConnectionString("piranha")));
                options.UseIdentity<IdentityPostgreSQLDb>(db =>
                    db.UseNpgsql(Configuration.GetConnectionString("piranha")));
                options.UseStartpageRouting = true;
            });

            services.AddDbContext<Db>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("piranha"));
            });

            services.AddScoped<DoctorService>();
            services.AddScoped<DoctorRepository>();
            services.AddScoped<CategoryService>();
            services.AddScoped<CategoryRepository>();
            services.AddScoped<ProductService>();
            services.AddScoped<ProductRepository>();
            services.AddScoped<PageRepository>();
            services.AddScoped<MedicalAidService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IApi api)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSerilogRequestLogging();

            // Initialize Piranha
            App.Init(api);

            // Configure cache level
            App.CacheLevel = Piranha.Cache.CacheLevel.Basic;

            // Build content types
            new ContentTypeBuilder(api)
                .AddAssembly(typeof(Startup).Assembly)
                .Build()
                .DeleteOrphans();
            
            App.Blocks.Register<YouTubeBlock>();
            App.Blocks.Register<WidgetBlock>();
            App.Blocks.Register<ProductBlock>();
            App.Blocks.Register<DoctorBlock>();

            ConfigureManagerScripts();
            ConfigureManagerMenu();

            // Configure Tiny MCE
            EditorConfig.FromFile("editorconfig.json");

            // Middleware setup
            app.UsePiranha(options => {
                options.UseManager();
                options.UseTinyMCE();
                options.UseIdentity();
            });
        }

        private void ConfigureManagerScripts()
        {
            var scripts = App.Modules.Manager().Scripts;
            scripts.Add("~/manager/js/components/product-block.js");
            scripts.Add("~/manager/js/components/doctor-block.js");
        }

        private void ConfigureManagerMenu()
        {
            Menu.Items.Insert(1, new MenuItem
            {
                InternalId = "MedicalModule",
                Name = "Медицина",
                Css = "fas fa-laptop-medical"
            });
            
            Menu.Items["MedicalModule"].Items.Add(new MenuItem
            {
                InternalId = "Products",
                Name = "Продукты",
                Route = "~/manager/products",
                Css = "fas fa-shopping-cart"
            });
            
            Menu.Items["MedicalModule"].Items.Add(new MenuItem
            {
                InternalId = "Categories",
                Name = "Категории",
                Route = "~/manager/categories",
                Css = "fas fa-tags"
            });
            

            Menu.Items["MedicalModule"].Items.Add(new MenuItem
            {
                InternalId = "Doctors",
                Name = "Врачи",
                Route = "~/manager/doctors",
                Css = "fas fa-user-md"
            });
        }
    }
}
