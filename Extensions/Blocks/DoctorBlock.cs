using Piranha.Extend;
using Piranha.Extend.Fields;

namespace Davinci.Extensions.Blocks
{
    [BlockType(Name = "Doctor", Component = "doctor-block", Category = "Medical", Icon = "fa fa-user-md")]
    public class DoctorBlock : Block
    {
        public TextField DoctorId { get; set; }

        public TextField Name { get; set; }

        public TextField About { get; set; }

        public TextField PageLink { get; set; }

        public TextField Photo { get; set; }
    }
}