using Piranha.Extend;
using Piranha.Extend.Fields;

namespace Davinci.Extensions.Blocks
{
    [BlockType(Name = "YouTube", Category = "Media", Icon = "fab fa-youtube")]
    public class YouTubeBlock : Block
    {
        public TextField Frame { get; set; }
    }
}