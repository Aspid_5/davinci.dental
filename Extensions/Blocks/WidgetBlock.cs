using Piranha.Extend;
using Piranha.Extend.Fields;

namespace Davinci.Extensions.Blocks
{
    [BlockType(Name = "Widget", Category = "Media", Icon = "far fa-file-code")]
    public class WidgetBlock : Block
    {
        public TextField Frame { get; set; }
    }
}