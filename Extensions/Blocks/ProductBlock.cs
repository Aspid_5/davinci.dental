using Piranha.Extend;
using Piranha.Extend.Fields;

namespace Davinci.Extensions.Blocks
{
    [BlockType(Name = "Product", Component = "product-block", Category = "Medical", Icon = "fa fa-shopping-cart")]
    public class ProductBlock : Block
    {
        public TextField ProductId { get; set; }
        
        public TextField Title { get; set; }

        public TextField Price { get; set; }

        public TextField MinimumPrice { get; set; }
        
        public TextField MaximumPrice { get; set; }
    }
}