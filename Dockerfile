FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src

COPY Davinci.csproj Davinci.csproj 
RUN dotnet restore Davinci.csproj

COPY . .
RUN dotnet publish -c Release -o published -f netcoreapp3.1

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS runtime
ENV ASPNETCORE_HTTPS_PORT=5003
ENV ASPNETCORE_URLS=http://0.0.0.0:5002;https://0.0.0.0:5003
WORKDIR /app
EXPOSE 5002 5003

COPY --from=build /src/published /app
COPY davinci.local.pfx /app/davinci.local.pfx

ENTRYPOINT ["dotnet", "Davinci.dll", "--launch-profile=Production"]