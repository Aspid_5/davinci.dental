using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Data;
using Davinci.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Davinci.Services
{
    public class ProductService
    {
        private readonly Db _db;

        public ProductService(Db db)
        {
            _db = db;
        }

        public async Task<Product> Save(Product product)
        {
            var isNew = false;
            var model = await _db.Products.FirstOrDefaultAsync(m => m.Id == product.Id);

            if (model == null)
            {
                isNew = true;
            }

            // Категории не должны быть измененными
            if (IsChanged(product.ProductCategories.Select(m => m.Category)))
            {
                throw new InvalidOperationException("Обнаружены изменения в категориях");
            }

            if (isNew)
            {
                product.CreatedAt = DateTime.Now;
                product.UpdatedAt = DateTime.Now;
                
                await _db.Products.AddAsync(product);
                await _db.SaveChangesAsync();

                return product;
            }

            await _db.Entry(model).Collection(m => m.ProductCategories).LoadAsync();
            _db.ProductCategories.RemoveRange(model.ProductCategories);
            await _db.SaveChangesAsync();

            model.Title = product.Title;
            model.Description = product.Description;
            model.Price = product.Price;
            model.MinimumPrice = product.MinimumPrice;
            model.MaximumPrice = product.MaximumPrice;
            model.UpdatedAt = DateTime.Now;
            model.ProductCategories = product.ProductCategories;

            await _db.SaveChangesAsync();

            return model;
        }

        public async Task Delete(Product product)
        {
            _db.Products.Remove(product);
            await _db.SaveChangesAsync();
        }

        /// <summary>
        /// Проверяет состояние категорий.
        /// Вернет истину, если категории изменялись во время выполнения.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private bool IsChanged(IEnumerable<Category> list)
        {
            var isChanged = false;

            foreach (var category in list)
            {
                var state = _db.Entry(category).State;

                if (state != EntityState.Unchanged)
                {
                    isChanged = true;
                }
            }

            return isChanged;
        }
    }
}