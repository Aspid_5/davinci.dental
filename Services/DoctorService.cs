using System;
using System.Threading.Tasks;
using Davinci.Data;
using Davinci.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Davinci.Services
{
    public class DoctorService
    {
        private readonly Db _db;

        public DoctorService(Db db)
        {
            _db = db;
        }

        public async Task<Doctor> Save(Doctor doctor)
        {
            var isNew = false;
            var model = await _db.Doctors.FirstOrDefaultAsync(m => m.Id == doctor.Id);

            if (model == null)
            {
                isNew = true;
            }

            if (isNew)
            {
                doctor.Id = Guid.NewGuid();

                await _db.Doctors.AddAsync(doctor);
                await _db.SaveChangesAsync();

                return doctor;
            }

            model.Name = doctor.Name;
            model.About = doctor.About;
            model.Photo = doctor.Photo;
            model.PageLink = doctor.PageLink;

            await _db.SaveChangesAsync();

            return doctor;
        }

        public async Task Delete(Doctor doctor)
        {
            _db.Doctors.Remove(doctor);
            await _db.SaveChangesAsync();
        }
    }
}