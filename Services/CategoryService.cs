using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Data;
using Davinci.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Davinci.Services
{
    public class CategoryService
    {
        private readonly Db _db;

        public CategoryService(Db db)
        {
            _db = db;
        }

        /// <summary>
        /// Добавление и изменение категории
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public async Task<Category> Save(Category category)
        {
            var isNew = false;
            var model = await _db.Categories.FirstOrDefaultAsync(m => m.Id == category.Id);

            if (model == null)
            {
                isNew = true;
            }

            if (isNew)
            {
                category.Id = Guid.NewGuid();

                if (category.SortOrder == 0)
                {
                    category.SortOrder = await _db.Categories
                        .CountAsync(m => m.ParentId == category.ParentId);                    
                }
                else
                {
                    var dest = await _db.Categories
                        .Where(m => m.ParentId == category.ParentId)
                        .ToListAsync();

                    MoveCategories(dest, category.SortOrder, true);
                }
                
                await _db.Categories.AddAsync(category);
                await _db.SaveChangesAsync();

                return category;
            }

            model.Title = category.Title;
            
            // Если страница была перемещена
            if (model.ParentId != category.ParentId || model.SortOrder != category.SortOrder)
            {
                var source = await _db.Categories
                    .Where(m => m.ParentId == model.ParentId && m.Id != model.Id)
                    .ToListAsync();

                var dest = model.ParentId == category.ParentId
                    ? source
                    : await _db.Categories.Where(m => m.ParentId == category.Id).ToListAsync();

                MoveCategories(source, model.SortOrder + 1, false);
                MoveCategories(dest, category.SortOrder, true);
            }

            await _db.SaveChangesAsync();

            return model;
        }

        /// <summary>
        /// Перемещение категории
        /// </summary>
        /// <param name="category">Объект переноса</param>
        /// <param name="parent">Родительская категория</param>
        /// <param name="sortOrder">Порядок при сортировке</param>
        /// <returns></returns>
        public async Task Move(Category category, Category parent, int sortOrder)
        {
            var source = await _db.Categories
                .Where(m => m.ParentId == category.ParentId && m.Id != category.Id)
                .ToListAsync();

            var parentId = parent?.Id;
            
            var dest = category.ParentId == parentId
                ? source
                : await _db.Categories.Where(m => m.ParentId == parentId).ToListAsync();
            
            MoveCategories(source, category.SortOrder + 1, false);
            MoveCategories(dest, sortOrder, true);

            category.SortOrder = sortOrder;
            category.ParentId = parent?.Id;

            await _db.SaveChangesAsync();
        }

        /// <summary>
        /// Удаление категории
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        public async Task Delete(Category category)
        {
            var childsCount = await _db.Categories.CountAsync(m => m.ParentId == category.Id);
            if (childsCount > 0)
            {
                throw new InvalidOperationException("Категория имеет потомков");
            }
            
            _db.Categories.Remove(category);

            var siblings = await _db.Categories
                .Where(m => m.ParentId == category.ParentId)
                .ToListAsync();
            
            MoveCategories(siblings, category.SortOrder + 1, false);

            await _db.SaveChangesAsync();
        }

        private static void MoveCategories(List<Category> categories, int sortOrder, bool increase)
        {
            var affected = categories.Where(m => m.SortOrder >= sortOrder).ToList();

            foreach (var category in affected)
            {
                category.SortOrder = increase ? category.SortOrder + 1 : category.SortOrder - 1;
            }
        }
    }
}