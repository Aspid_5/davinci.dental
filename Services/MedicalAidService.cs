using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Repositories;
using Piranha.Data;

namespace Davinci.Services
{
    public class MedicalAidService
    {
        private readonly PageRepository _pageRepository;

        public MedicalAidService(PageRepository pageRepository)
        {
            _pageRepository = pageRepository;
        }
        
        /// <summary>
        /// Получение всех страниц помощи в виде иерархии
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<Page, IList<Page>>> GetAidHierarchy()
        {
            var pages = await _pageRepository.GetMedicalAidPages();

            var root = pages.Single(p => !p.ParentId.HasValue);
            var parents = pages.Where(p => p.ParentId.Equals(root.Id));
            var childs = pages.Where(p => p.ParentId.HasValue);

            var hierarchy = new Dictionary<Page, IList<Page>>();
            
            foreach (var parent in parents)
            {
                hierarchy.Add(parent, childs.Where(c => c.ParentId.Equals(parent.Id)).ToList());
            }

            return hierarchy;
        }

        /// <summary>
        /// Получение корневой страницы помощи
        /// </summary>
        /// <returns></returns>
        public async Task<Page> GetAidRoot()
        {
            var pages = await _pageRepository.GetMedicalAidPages();

            return pages.Single(p => !p.ParentId.HasValue);
        }
    }
}