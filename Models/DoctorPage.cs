﻿using Davinci.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Extend;
using Piranha.Models;
using System.Collections.Generic;
using Piranha.Extend.Fields;

namespace Davinci.Models
{
	[PageType(Title = "Врач")]
	[PageTypeRoute(Title = "Default", Route = "/doctor")]
	public class DoctorPage : Page<DoctorPage> 
	{
		[Region(Title = "Детали")]
		public Doctor Doctor { get; set; }
		
		[Region(Title = "Сертификаты")]
		public IList<ImageField> Certificates { get; set; }
		
		public DoctorPage ()
		{
			Doctor = new Doctor();
			Certificates = new List<ImageField>();
		}
	}
}