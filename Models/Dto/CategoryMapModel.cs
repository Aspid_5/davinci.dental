using System;
using System.Collections.Generic;
using System.Linq;
using Davinci.Data.Models;

namespace Davinci.Models.Dto
{
    public class CategoryMapModel
    {
        public Guid Id { get; set; }

        public string Title { get; set; }
        
        public int SortOrder { get; set; }
        
        public ICollection<CategoryMapModel> Items { get; set; }

        public CategoryMapModel(Guid id, string title, int sortOrder)
        {
            Id = id;
            Title = title;
            SortOrder = sortOrder;
            Items = new List<CategoryMapModel>();
        }

        /// <summary>
        /// Рекурсивно строит дерево категорий
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        public static CategoryMapModel BuildMap(Category parent, List<Category> categories)
        {
            var parentNode = new CategoryMapModel(parent.Id, parent.Title, parent.SortOrder);

            foreach (var current in categories.Where(current => current.ParentId == parentNode.Id))
            {
                parentNode.Items.Add(BuildMap(current, categories));
            }

            return parentNode;
        }

    }
}