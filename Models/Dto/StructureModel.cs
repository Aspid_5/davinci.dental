using System;
using System.Collections.Generic;

namespace Davinci.Models.Dto
{
    /// <summary>
    /// Используется для маппинга стуктур, формируемых плагином jquery.nestable
    /// </summary>
    public class StructureModel
    {
        public class Item
        {
            public Guid Id { get; set; }
            
            public IList<Item> Children { get; set; }
            
            public Item()
            {
                Children = new List<Item>();
            }
        }
        
        /// <summary>
        /// Id перемещаемого объекта
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Новая структура
        /// </summary>
        public IList<Item> Items { get; set; } = new List<Item>();
    }
}