using System;

namespace Davinci.Models.Dto
{
    public class CategoryModel
    {
        public Guid? Id { get; set; }
        
        public string Title { get; set; }
        
        public Guid? ParentId { get; set; }
        
        public int SortOrder { get; set; }
    }
}