using System;
using System.Collections.Generic;

namespace Davinci.Models.Dto
{
    public class ProductModel
    {
        public class CategoryItem
        {
            public Guid Id { get; set; }
            
            public string Title { get; set; }

            public CategoryItem(Guid id, string title)
            {
                Id = id;
                Title = title;
            }
        }

        public Guid? Id { get; set;  }

        public string Title { get; set; }

        public string Description { get; set; }
        
        public decimal? Price { get; set; }
        
        public decimal? MinimumPrice { get; set; }
        
        public decimal? MaximumPrice { get; set; }
        
        public DateTime? CreatedAt { get; set; }
        
        public DateTime? UpdatedAt { get; set; }

        public IEnumerable<CategoryItem> Categories { get; set; }

        public ProductModel()
        {
            Categories = new List<CategoryItem>();
        }
    }
}