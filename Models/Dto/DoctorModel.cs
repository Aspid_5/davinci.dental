using System;

namespace Davinci.Models.Dto
{
    public class DoctorModel
    {
        public Guid? Id { get; set; }

        public string Name { get; set; }

        public string About { get; set; }

        public Guid PageLink { get; set; }

        public string PageLinkUrl { get; set; }

        public Guid Photo { get; set; }

        public string PhotoUrl { get; set; }
    }
}