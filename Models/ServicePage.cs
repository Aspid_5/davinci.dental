using Piranha.AttributeBuilder;
using Piranha.Models;

namespace Davinci.Models
{
    [PageType(Title = "Услуги")]
    [PageTypeRoute(Title = "Default", Route = "/service")]
    public class ServicePage : Page<ServicePage>
    {
    }
}