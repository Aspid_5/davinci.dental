using System;
using System.Collections.Generic;
using Davinci.Data.Models;
using Piranha.AttributeBuilder;
using Piranha.Models;

namespace Davinci.Models
{
    [PageType(Title = "Цены")]
    [PageTypeRoute(Title = "Default", Route = "/price")]
    public class PricePage : Page<PricePage>
    {
        public CategoryMap Categories { get; set; }

        public Dictionary<Guid, List<Product>> Products { get; set; }

        public PricePage()
        {
            Categories = new CategoryMap();
            Products = new Dictionary<Guid, List<Product>>();
        }
    }
}