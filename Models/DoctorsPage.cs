﻿using Piranha.AttributeBuilder;
using Piranha.Models;

namespace Davinci.Models
{
	[PageType(Title = "Врачи")]
	[PageTypeRoute(Title = "Default", Route = "/doctors")]
	public class DoctorsPage : Page<DoctorsPage> 
	{
	}
}