using Piranha.Extend;
using Piranha.Extend.Fields;
using Piranha.Models;

namespace Davinci.Models.Regions
{
    public class Teaser
    {
        [Field(Options = FieldOption.HalfWidth, Title = "Заголовок")]
        public StringField Title { get; set; }

        [Field(Options = FieldOption.HalfWidth, Title = "Подзаголовок")]
        public StringField SubTitle { get; set; }
        
        [Field(Title = "Опциональная ссылка на страницу")]
        public PageField PageLink { get; set; }

        [Field(Title = "Фоновое изображение")]
        public ImageField Background { get; set; }

        public Teaser() {
            PageLink = new PageField();
        }
    }
}
