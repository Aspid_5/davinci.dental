using Piranha.Extend;
using Piranha.Extend.Fields;

namespace Davinci.Models.Regions
{
    public class Doctor
    {
        [Field(Title = "Имя")]
        public StringField Name { get; set; }

        [Field(Title = "Описание")]
        public HtmlField Description { get; set; }
        
        [Field(Title = "Ссылка на страницу")]
        public PageField PageLink { get; set; }

        [Field(Title = "Фото врача")]
        public ImageField Photo { get; set; }

        public Doctor() {
            PageLink = new PageField();
        }
    }
}
