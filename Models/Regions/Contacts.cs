using Piranha.Extend;
using Piranha.Extend.Fields;
using Piranha.Models;

namespace Davinci.Models.Regions
{
    public class Contacts
    {
        [Field(Title = "Адрес")]
        public StringField Address { get; set; }
        
        [Field(Options = FieldOption.HalfWidth, Title = "Телефон")]
        public StringField Phone { get; set; }
        
        [Field(Options = FieldOption.HalfWidth, Title = "Почта")]
        public StringField Email { get; set; }
        
        [Field(Options = FieldOption.HalfWidth, Title = "График работы")]
        public HtmlField WorkTime { get; set; }
    }
}