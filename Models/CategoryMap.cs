using System.Collections.Generic;
using System.Linq;
using Davinci.Data.Models;

namespace Davinci.Models
{
    public class CategoryMap
    {
        public Category Current { get; set; }
        
        public ICollection<CategoryMap> Childs { get; set; }

        public CategoryMap()
        {
            Childs = new List<CategoryMap>();
        }

        public CategoryMap(Category current) : this()
        {
            Current = current;
        }

        public static CategoryMap BuildMap(IEnumerable<Category> categories)
        {
            return BuildMap(null, categories);
        }

        private static CategoryMap BuildMap(Category parent, IEnumerable<Category> categories)
        {
            var parentNode = new CategoryMap(parent);

            foreach (var current in categories.Where(current => current.ParentId == parent?.Id))
            {
                parentNode.Childs.Add(BuildMap(current, categories));
            }

            return parentNode;
        }
    }
}