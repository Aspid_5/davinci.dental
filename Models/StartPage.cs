using Davinci.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Extend;
using Piranha.Models;
using System.Collections.Generic;

namespace Davinci.Models
{
    [PageType(Title = "Стартовая")]
    [PageTypeRoute(Title = "Default", Route = "/start")]
    public class StartPage : Page<StartPage>
    {
        [Region(ListTitle = "Title")]
        public IList<Teaser> Teasers { get; set; }

        public StartPage() {
            Teasers = new List<Teaser>();
        }
    }
}