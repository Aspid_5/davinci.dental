using System;

namespace Davinci.Models.ReCaptcha
{
    public class ResponseV3
    {
        public bool Success { get; set; }

        public DateTime ChallengeTs { get; set; }
        
        public string Hostname { get; set; }
        
        public double Score { get; set; }
        
        public string Action { get; set; }
    }
}