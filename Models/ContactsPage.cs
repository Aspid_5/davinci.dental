using Davinci.Models.Regions;
using Piranha.AttributeBuilder;
using Piranha.Extend;
using Piranha.Models;

namespace Davinci.Models
{
    [PageType(Title = "Контакты")]
    [PageTypeRoute(Title = "Default", Route = "/contacts")]
    public class ContactsPage : Page<ContactsPage>
    {
        [Region(Title = "Контакты")]
        public Contacts Contacts { get; set; }
    }
}