using Microsoft.AspNetCore.Mvc;

namespace Davinci.Controllers
{
    public class HomeController : Controller
    {
        [Route("/Privacy", Name = "privacy")]
        public IActionResult Privacy()
        {
            return View();
        }
    }
}