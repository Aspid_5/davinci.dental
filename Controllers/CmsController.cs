﻿using Davinci.Models;
using Microsoft.AspNetCore.Mvc;
using Piranha.AspNetCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Davinci.Data.Models;
using Davinci.Data.Repositories;
using Davinci.Extensions.Blocks;
using Piranha;

namespace Davinci.Controllers
{
    public class CmsController : Controller
    {
        private readonly IApi _api;

        private readonly IModelLoader _loader;

        private readonly CategoryRepository _categoryRepository;

        private readonly ProductRepository _productRepository;

        private readonly DoctorRepository _doctorRepository;

        public CmsController(IApi api,
            IModelLoader loader,
            CategoryRepository categoryRepository,
            ProductRepository productRepository,
            DoctorRepository doctorRepository)
        {
            _api = api;
            _loader = loader;
            _categoryRepository = categoryRepository;
            _productRepository = productRepository;
            _doctorRepository = doctorRepository;
        }

        /// <summary>
        /// Gets the page with the given id.
        /// </summary>
        /// <param name="id">The unique page id</param>
        /// <param name="draft">If a draft is requested</param>
        [Route("page")]
        public async Task<IActionResult> Page(Guid id, bool draft = false)
        {
            var model = await _loader.GetPageAsync<StandardPage>(id, HttpContext.User, draft);

            return View(model);
        }

        /// <summary>
        /// Gets the startpage with the given id.
        /// </summary>
        /// <param name="id">The unique page id</param>
        /// <param name="draft">If a draft is requested</param>
        [Route("start")]
        public async Task<IActionResult> Start(Guid id, bool draft = false)
        {
            var model = await _loader.GetPageAsync<StartPage>(id, HttpContext.User, draft);

            return View(model);
        }

        [Route("price")]
        public async Task<IActionResult> Price(Guid id, bool draft = false)
        {
            var model = await _loader.GetPageAsync<PricePage>(id, HttpContext.User, draft);

            var categories = await _categoryRepository.GetList();
            var products = await _productRepository.GetList(null, new Options {FullEntry = true});

            model.Categories = CategoryMap.BuildMap(categories);
            model.Products = BuildProductDictionary(products);

            return View(model);
        }

        [Route("service")]
        public async Task<IActionResult> Service(Guid id, bool draft = false)
        {
            var model = await _loader.GetPageAsync<ServicePage>(id, HttpContext.User, draft);

            var productBlocks = model.Blocks
                .Where(m => m is ProductBlock)
                .Cast<ProductBlock>()
                .ToList();

            await LoadBlockContent(productBlocks);

            var doctorBlocks = model.Blocks
                .Where(m => m is DoctorBlock)
                .Cast<DoctorBlock>()
                .ToList();

            await LoadBlockContent(doctorBlocks);

            return View(model);
        }

        [Route("contacts")]
        public async Task<IActionResult> Contacts(Guid id, bool draft = false)
        {
            var model = await _loader.GetPageAsync<ContactsPage>(id, HttpContext.User, draft);

            return View(model);
        }

        [Route("doctor")]
        public async Task<IActionResult> Doctor(Guid id, bool draft = false)
        {
            var model = await _loader.GetPageAsync<DoctorPage>(id, HttpContext.User, draft);

            return View(model);
        }

        [Route("doctors")]
        public async Task<IActionResult> Doctors(Guid id, bool draft = false)
        {
            var model = await _loader.GetPageAsync<DoctorsPage>(id, HttpContext.User, draft);

            var doctorBlocks = model.Blocks
                .Where(m => m is DoctorBlock)
                .Cast<DoctorBlock>()
                .ToList();

            await LoadBlockContent(doctorBlocks);

            return View(model);
        }

        private Dictionary<Guid, List<Product>> BuildProductDictionary(List<Product> products)
        {
            var result = new Dictionary<Guid, List<Product>>();

            foreach (var product in products)
            {
                foreach (var productCategory in product.ProductCategories)
                {
                    var key = productCategory.CategoryId;
                    if (!result.ContainsKey(key))
                    {
                        result.Add(key, new List<Product>());
                    }

                    result[key].Add(product);
                }
            }

            return result;
        }

        private async Task LoadBlockContent(List<ProductBlock> blocks)
        {
            if (!blocks.Any())
            {
                return;
            }

            var products = await _productRepository.GetList();

            const string notAvailable = "[Продукт не доступен]";

            foreach (var block in blocks)
            {
                Guid productId;

                try
                {
                    productId = Guid.Parse(block.ProductId.Value);
                }
                catch (ArgumentNullException)
                {
                    block.Title = notAvailable;
                    continue;
                }
                catch (FormatException)
                {
                    block.Title = notAvailable;
                    continue;
                }

                var product = products.SingleOrDefault(m => m.Id == productId);

                if (product == null)
                {
                    block.Title = notAvailable;
                    continue;
                }

                block.Title = product.Title;
                block.Price = product.Price.ToString();
                block.MinimumPrice = product.MinimumPrice.ToString();
                block.MaximumPrice = product.MaximumPrice.ToString();
            }
        }

        private async Task LoadBlockContent(List<DoctorBlock> blocks)
        {
            if (!blocks.Any())
            {
                return;
            }

            var doctors = await _doctorRepository.GetList();

            const string notAvailable = "[Врач не доступен]";

            foreach (var block in blocks)
            {
                Guid doctorId;

                try
                {
                    doctorId = Guid.Parse(block.DoctorId.Value);
                }
                catch (ArgumentNullException)
                {
                    block.Name = notAvailable;
                    continue;
                }
                catch (FormatException)
                {
                    block.Name = notAvailable;
                    continue;
                }

                var doctor = doctors.SingleOrDefault(m => m.Id == doctorId);
                if (doctor == null)
                {
                    block.Name = notAvailable;
                    continue;
                }

                var page = await _api.Pages.GetByIdAsync(doctor.PageLink);
                var photo = await _api.Media.GetByIdAsync(doctor.Photo);

                block.Name = doctor.Name;
                block.About = doctor.About;
                block.PageLink = page?.Slug;
                block.Photo = photo?.PublicUrl.Replace("~", "");
            }
        }
    }
}