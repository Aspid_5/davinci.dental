using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Davinci.Controllers
{
    public class LikeDoctorController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Works()
        {
            const string uri = "https://like.doctor/widgets/lenta" +
                               "?mode=clinic&sid=0&type=2&id=61320&perpage=10&origin=https://davinci.dental";

            const string referrer = "https://davinci.dental";

            var content = await ParseWidget(uri, referrer);
            if (string.IsNullOrEmpty(content))
            {
                return Ok("Виджет временно недоступен.");
            }

            var indexOfHead = content.IndexOf("</head>");
            if (indexOfHead != -1)
            {
                content = content.Insert(indexOfHead,
                    //"<base href=\"https://like.doctor/\">" +
                    "<link href=\"/css/likedoctor-works.css\" rel=\"stylesheet\">"
                );
            }

            return new ContentResult
            {
                Content = content,
                ContentType = "text/html"
            };
        }

        public async Task<IActionResult> Reviews()
        {
            const string uri = "https://like.doctor/widgets/lenta" +
                               "?mode=clinic&sid=0&type=1&id=61320&perpage=10&origin=https://davinci.dental";

            const string referrer = "https://davinci.dental";

            var content = await ParseWidget(uri, referrer);
            if (string.IsNullOrEmpty(content))
            {
                return Ok("Виджет временно недоступен.");
            }

            var indexOfHead = content.IndexOf("</head>");
            if (indexOfHead != -1)
            {
                content = content.Insert(indexOfHead,
                    "<link href=\"/css/likedoctor-reviews.css\" rel=\"stylesheet\">"
                );
            }

            return new ContentResult
            {
                Content = content,
                ContentType = "text/html"
            };
        }

        private static async Task<string> ParseWidget(string uri, string referrer)
        {
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Referrer = new Uri(referrer);
            var result = await httpClient.GetAsync(uri);

            if (result.StatusCode != HttpStatusCode.OK) return string.Empty;

            var content = await result.Content.ReadAsStringAsync();

            // Исправляем проблему с CORS при загрузке шрифта из css
            content = content.Replace("<link href=\"/css/font-awesome.min.css\" rel=\"stylesheet\">",
                "<link href=\"https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\">");

            // Ссылки на клинку открывам в новой вкладке
            content = content.Replace("<a href=\"/voronezh/kliniki/stomatologiya-davinchi\">",
                "<a href=\"/voronezh/kliniki/stomatologiya-davinchi\" target=\"_blank\">");

            // Ссылки на отызвы открываем в новой вкладке
            content = content.Replace("href=\"/voronezh/otzyv", " target=\"_blank\" href=\"/voronezh/otzyv");

            content = content.Replace("href=\"/", "href=\"https://like.doctor/");
            content = content.Replace("src=\"/", "src=\"https://like.doctor/");
            content = content.Replace("/service/more-posts", "https://like.doctor/service/more-posts");

            return content;
        }
    }
}