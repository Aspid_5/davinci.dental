using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Davinci.Models.ReCaptcha;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace Davinci.Controllers
{
    public class ContactController : Controller
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Contact(string name, string phone, string message, string recaptcha)
        {
            if (!await IsHuman(recaptcha))
            {
                return Ok("bot");
            }

            var status = await SendMail("Связь с дирекцией", name + ", " + phone + "\n" + message);

            return status ? Ok("ok") : Ok("error");
        }

        private async Task<bool> SendMail(string subject, string message)
        {
            const string apiKey = "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";

            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("robot.native@gmail.com");
            var to = new EmailAddress("davinci.vrn@gmail.com");
            var msg = MailHelper.CreateSingleEmail(from, to, subject, message, "");
            var response = await client.SendEmailAsync(msg);

            return response.StatusCode == HttpStatusCode.Accepted;
        }

        private static async Task<bool> IsHuman(string recaptcha)
        {
            const string uri = "https://www.google.com/recaptcha/api/siteverify";

            var request = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("secret", "6LdSWv8UAAAAAJDJd3iovaoj2TnHC03mMrHgzI5B"),
                new KeyValuePair<string, string>("response", recaptcha),
            });

            var httpClient = new HttpClient();
            var result = await httpClient.PostAsync(uri, request);

            // Если гугл не работает, считаем что капча пройдена
            if (result.StatusCode != HttpStatusCode.OK) return true;

            var content = await result.Content.ReadAsStringAsync();
            var response = JsonConvert.DeserializeObject<ResponseV3>(content);

            return response.Success && response.Score > 0.5;
        }
    }
}