# Davinci.dental

## Сборка окружения разработчика

1 Сборка и запуск окружения:

    docker-compose build

затем

    docker-compose up -d
    
2 Переименуйте appsettings.Development.dist.json в appsettings.Development.json, проверьте корректность последнего.

3 Соберите и запустите приложение.

    dotnet run --launch-profile=Development

4 Выполните миграции:

    dotnet ef database update --context=Db

5 Заполнение базовым контентом. Выполните sql-скрипты в следующем порядке:

- Piranha_Blocks.sql
- Piranha_BlockFields.sql
- Piranha_Sites.sql
- Piranha_MediaFolders.sql
- Piranha_Media.sql
- Piranha_MediaVersions.sql
- Piranha_Pages.sql
- Piranha_PageBlocks.sql
- Piranha_PageFields.sql
- update-default-site.sql
- Categories.sql
- Products.sql
- ProductCategories.sql

6 Перезапустите проект. Проект готов к работе. 

Админка находится по адресу `https://localhost:5001/manager`.

## Сборка ассетов

1 Сборка образа:

    docker build -t front-builder docker/front-builder/
    
2 Запуск сборщика ассетов:

    docker run -it -v "$PWD"/assets:/app/assets -v "$PWD"/wwwroot:/app/wwwroot front-builder gulp min:css

Так же доступен запуск сборщика в режиме отслеживания изменений:

    docker run -it -v "$PWD"/assets:/app/assets -v "$PWD"/wwwroot:/app/wwwroot front-builder gulp watch

## Добавление новой миграции

    dotnet ef migrations add --context=Db --output-dir=Data/Migrations MigrationName

Где `MigrationName` имя миграции.

# Получение SQL схемы

    dotnet ef migrations script --context=Db <миграция, с которой генерировать SQL>

# Деплой релиза

    ansible-playbook -i inventory deploy.yml --extra-vars "version=x.y.z"

Где `x.y.z` номер версии.
