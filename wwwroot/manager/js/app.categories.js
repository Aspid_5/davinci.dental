/*global
    piranha
*/

piranha.Categories = new Vue({
    el: '#categories',
    data: {
        list: [],
        loading: true,
        updateBindings: false
    },
    methods: {
        load: function () {
            let self = this;

            fetch( '/manager/api/category/list')
                .then(function (response) {
                    return response.json();
                })
                .then(function (result) {
                    self.list = result;
                })
                .catch(function (error) { console.log("error:", error ); });
        },
        remove: function (id) {
            let self = this;

            fetch("/manager/api/category/delete/" + id, {
                method: "post"
            })
                .then(function (response) {
                    if (response.status === 200) {
                        self.load();
                    }
                })
                .catch(function (error) { console.log("error:", error ); });

        },
        bind: function () {
            let self = this;
            let list = $(this.$el).find('#categories-list');

            list.nestable({
                maxDepth: 10,
                group: 1,
                callback: function (l, e) {
                    let data = {
                        id: e.attr("data-id"),
                        items: list.nestable("serialize")
                    };

                    list.nestable("destroy");

                    fetch("/manager/api/category/move", {
                        method: "post",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(data)
                    })
                        .then(function (response) {
                            if (response.status === 200) {
                                self.updateBindings = false;

                                Vue.nextTick(function () {
                                    self.load();

                                    Vue.nextTick(function () {
                                        self.bind();
                                    })
                                });
                            }
                        })
                        .catch(function (error) { console.log("error:", error ); });
                }
            });
        }
    },
    created: function () {
        this.updateBindings = true;
        this.load();
    },
    updated: function () {
        this.loading = false;
        
        if (this.updateBindings) {
            this.bind();
            this.updateBindings = false;
        }
    }
});