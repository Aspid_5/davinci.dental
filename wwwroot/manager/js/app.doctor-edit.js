/*global
    piranha
*/

piranha.DoctorEdit = new Vue({
    el: '#doctor-edit',
    data: {
        id: null,
        name: '',
        about: '',
        pageLink: '',
        pageLinkUrl: '',
        photo: '',
        photoUrl: '',
        callback: null,
        isNew: false
    },
    methods: {
        load: function (id) {
            let self = this;

            fetch("/manager/api/doctor/" + id)
                .then(function (response) { return response.json(); })
                .then(function (result) {
                    self.id = result.id;
                    self.name = result.name;
                    self.about = result.about;
                    self.pageLink = result.pageLink;
                    self.pageLinkUrl = result.pageLinkUrl;
                    self.photo = result.photo;
                    self.photoUrl = result.photoUrl;

                    self.isNew = false;
                });
        },
        save: function () {
            let self = this;

            let model = {
                id: this.id,
                name: this.name,
                about: this.about,
                pageLink: this.pageLink,
                photo: this.photo,
            };

            fetch("/manager/api/doctor/save", {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(model)
            })
                .then(function (response) {
                    if (response.status === 200) {
                        $(self.$el).modal('hide');
                        self.processCallback();
                    }
                })
                .catch(function (error) { console.log("error:", error ); });
        },
        open: function (id, callback) {
            this.callback = callback;
            this.load(id);
            $(this.$el).modal('show');
        },
        create: function (callback) {
            this.callback = callback;
            this.isNew = true;

            this.id = null;
            this.name = '';
            this.about = '';
            this.pageLink = '';
            this.pageLinkUrl = '';
            this.photo = '';
            this.photoUrl = '';

            $(this.$el).modal('show');
        },
        remove: function () {
            let self = this;

            fetch("/manager/api/doctor/delete/" + self.id, {
                method: "post"
            })
                .then(function (response) {
                    if (response.status === 200) {
                        $(self.$el).modal('hide');
                        self.processCallback();
                    }
                })
                .catch(function (error) { console.log("error:", error ); });

        },
        processCallback: function () {
            if (this.callback) {
                this.callback();
                this.callback = null;
            }
        },
        selectPhoto : function () {
            piranha.mediapicker.open(this.onSelectedPhoto, "Image");
        },
        onSelectedPhoto: function (media) {
            if (media.type === "Image") {
                this.photo = media.id;
                this.photoUrl = media.publicUrl;
            } else {
                console.log("No image was selected");
            }
        },
        selectPageLink: function () {
            let davinciId = 'd2066d0c-9957-48dc-aaae-a8239f58ad4f'
            piranha.pagepicker.open(this.onSelectedPage, davinciId);
        },
        onSelectedPage: function (page) {
            if (page) {
                this.pageLink = page.id;
                this.pageLinkUrl = page.permalink;
            }
        }
    }
});