/*global
    piranha
*/

piranha.CategoryEdit = new Vue({
    el: '#category-edit',
    data: {
        id: null,
        title: '',
        parentId: null,
        sortOrder: 0,
        callback: null, // Вызывается при операциях модификации категории
        isNew: false
    },
    methods: {
        load: function (id) {
            let self = this;

            fetch("/manager/api/category/" + id)
                .then(function (response) { return response.json(); })
                .then(function (result) {
                    self.id = result.id;
                    self.title = result.title;
                    self.parentId = result.parentId;
                    self.sortOrder = result.sortOrder;
                    self.isNew = false;
                });
        },
        save: function () {
            let self = this;

            let model = {
                id: this.id,
                title: this.title,
                parentId: this.parentId,
                sortOrder: this.sortOrder
            };

            fetch("/manager/api/category/save", {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(model)
            })
                .then(function (response) {
                    if (response.status === 200) {
                        $(self.$el).modal('hide');
                        self.processCallback();
                    }
                })
                .catch(function (error) { console.log("error:", error ); });
        },
        open: function (id, callback) {
            this.callback = callback;
            this.load(id);
            $(this.$el).modal('show');
        },
        create: function (parentId, sortOrder, callback) {
            this.id = null;
            this.title = "";
            this.parentId = parentId;
            this.sortOrder = sortOrder;
            this.callback = callback;
            this.isNew = true;

            $(this.$el).modal('show');
        },
        processCallback: function () {
            if (this.callback) {
                this.callback();
                this.callback = null;
            }
        }
    }
});