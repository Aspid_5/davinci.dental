/*global
    piranha
*/

piranha.Doctors = new Vue({
    el: '#doctors',
    data: {
        list: [],
        loading: true
    },
    methods: {
        load: function () {
            let self = this;

            fetch( '/manager/api/doctor/list')
                .then(function (response) {
                    return response.json();
                })
                .then(function (result) {
                    self.list = result;
                })
                .catch(function (error) { console.log("error:", error ); });
        },
    },
    created: function () {
        this.load();
    },
    updated: function () {
        this.loading = false;
    }
});