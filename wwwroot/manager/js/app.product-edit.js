/*global
    piranha
*/

piranha.ProductEdit = new Vue({
   el: '#product-edit',
   data: {
       id: null,
       title: '',
       price: null,
       minimumPrice: null,
       maximumPrice: null,
       description: null,
       categories: [],
       categoryCatalog: [],
       callback: null, // Вызывается при операциях модификации продукта
       isNew: false
   },
    methods: { 
        load: function (id) {
            let self = this;
            
            fetch("/manager/api/product/" + id)
                .then(function (response) { return response.json(); })
                .then(function (result) {
                    self.id = result.id;
                    self.title = result.title;
                    self.price = result.price;
                    self.minimumPrice = result.minimumPrice;
                    self.maximumPrice = result.maximumPrice;
                    self.description = result.description;
                    self.categories = result.categories;
                    self.isNew = false;
                });
        },
        loadCategories: function() {
            let self = this;
            
            fetch("/manager/api/category/list")
                .then(function (response) { return response.json(); })
                .then(function (result) {
                    self.categoryCatalog = result;
                });
        },
        save: function () {
            let self = this;
            
            let model = {
                id: this.id,
                title: this.title,
                price: this.price,
                minimumPrice: this.minimumPrice,
                maximumPrice: this.maximumPrice,
                description: this.description,
                categories: this.categories
            };
            
            fetch("/manager/api/product/save", {
                method: "post",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(model)
            })
                .then(function (response) {
                    if (response.status === 200) {
                        $(self.$el).modal('hide');
                        self.processCallback();
                    }
                })
                .catch(function (error) { console.log("error:", error ); });
        },
        open: function (id, callback) {
            this.callback = callback;
            this.load(id);
            this.loadCategories();
            $(this.$el).modal('show');
        },
        create: function (callback) {
            this.callback = callback;
            this.isNew = true;
            this.id = null;
            this.title = '';
            this.price = null;
            this.minimumPrice = null;
            this.maximumPrice = null;
            this.description = null;
            this.categories = [];
            
            this.loadCategories();
            
            $(this.$el).modal('show');
        },
        remove: function () {
            let self = this;
            
            fetch("/manager/api/product/delete/" + self.id, {
                method: "post"
            })
                .then(function (response) {
                    if (response.status === 200) {
                        $(self.$el).modal('hide');
                        self.processCallback();
                    }
                })
                .catch(function (error) { console.log("error:", error ); });
            
        },
        processCallback: function () {
            if (this.callback) {
                this.callback();
                this.callback = null;
            }
        },
        addCategory: function(event) {
            let self = this;
            
            if (event.target.value.length > 0) {
                let id = event.target.value;

                this.categoryCatalog.find(function (item) {
                    if (item.id === id) {
                        self.categories.push({
                           id: id,
                           title: item.title 
                        });
                    }
                });

                event.target.value = ""; 
            }
        },
        removeCategory: function (id) {
            let position = null;

            this.categories.find(function (item, index) {
                if (item.id === id) {
                    position = index;
                }
            });

            this.categories.splice(position, 1);
        }
    }
});