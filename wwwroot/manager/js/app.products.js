/*global
    piranha
*/

piranha.Products = new Vue({
    el: '#products',
    data: {
        list: [],
        filter: {
            categoryId: null
        },
        categoryCatalog: [],
        loading: true
    },
    methods: {
        load: function (categoryId) {
            let self = this;
            
            let queryParams = "";
            this.filter.categoryId = null;

            if (categoryId) {
                queryParams = "?categoryId=" + encodeURIComponent(categoryId);
                this.filter.categoryId = categoryId; 
            }

            fetch( '/manager/api/product/list' + queryParams)
                .then(function (response) {
                    return response.json();
                })
                .then(function (result) {
                    self.list = result;
                })
                .catch(function (error) { console.log("error:", error ); });
        },
        loadCategories: function() {
            let self = this;

            fetch("/manager/api/category/list")
                .then(function (response) { return response.json(); })
                .then(function (result) {
                    self.categoryCatalog = result;
                });
        },
    },
    created: function () {
        this.load();
        this.loadCategories();
    },
    updated: function () {
        this.loading = false;
    },
    filters: {
        date: function (date) {
            let format = function (date) {
                let day = date.getDate().toString();
                let month = (date.getMonth() + 1).toString();
                let year = date.getFullYear();
                let hours = date.getHours().toString();
                let minutes = date.getMinutes().toString();

                return day.padStart(2, "0") + '.' + month.padStart(2, "0") + '.' + year + ' ' + hours + ':' + minutes.padStart(2, "0");
            }

            if (typeof date === "string") {
                return format(new Date(date));
            }

            if (date instanceof Date) {
                return format(date);    
            }

            return '';
        },
        
        price: function (price) {
            let format = function (price) {
                return price.toString() + ' ₽'
            }
            
            if (price) {
                return format(price);
            }
            
            return '';
        },

        minimumPrice: function (minimumPrice) {
            let format = function (minimumPrice) {
                return 'от '+ minimumPrice.toString() + ' ₽'
            }

            if (minimumPrice) {
                return format(minimumPrice);
            }

            return '';
        },

        maximumPrice: function (maximumPrice) {
            let format = function (maximumPrice) {
                return 'до ' + maximumPrice.toString() + ' ₽'
            }

            if (maximumPrice) {
                return format(maximumPrice);
            }

            return '';
        }
    }
});