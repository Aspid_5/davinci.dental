Vue.component('category-item', {
    props: ['item', 'parent'],
    template: `
<li class="dd-item" :data-id="item.id">
    <div class="category-item">
        <div class="handle dd-handle">
            <i class="fas fa-ellipsis-v"></i>
        </div>
        <div class="link">
            <a href="#" v-on:click.prevent="piranha.CategoryEdit.open(item.id, piranha.Categories.load)">{{ item.title }}</a>
        </div>
        <div class="actions">
            <a v-on:click.prevent="piranha.CategoryEdit.create(parent.id, item.sortOrder + 1, piranha.Categories.load)"><i class="fas fa-angle-down"></i></a>
            <a v-on:click.prevent="piranha.CategoryEdit.create(item.id, 0, piranha.Categories.load)"><i class="fas fa-angle-right"></i></a>
            <a v-if="item.items.length === 0" v-on:click.prevent="piranha.Categories.remove(item.id)" class="danger" href="#"><i class="fas fa-trash"></i></a>
        </div>
    </div>
    <ol v-if="item.items.length > 0" class="dd-list">
        <category-item v-for="child in item.items" :key="child.id" :item="child" :parent="item"></category-item>
    </ol>
</li>
    `
});