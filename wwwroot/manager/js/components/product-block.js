Vue.component("product-block", {
    props: ["uid", "model"],
    data: function () {
        return {
            title: '',
            products: [],
            isNew: true
        };
    },
    methods: {
        loadProduct: function (id) {
            let self = this;
            
            fetch("/manager/api/product/" + id)
                .then(function (response) { return response.json(); })
                .then(function (result) {
                    self.title = result.title;
                    self.isNew = false;
                });
        },
        loadProducts: function () {
            let self = this;

            fetch( '/manager/api/product/list')
                .then(function (response) {
                    return response.json();
                })
                .then(function (result) {
                    self.products = result;
                })
                .catch(function (error) { console.log("error:", error ); });
        },
        addProduct: function (event) {
            let id = event.target.value;
            
            if (id.length > 0) {
                this.model.productId.value = id;
                this.loadProduct(id);
            }
        }
    },
    created: function () {
        if (this.model.productId.value) {
            this.loadProduct(this.model.productId.value);
            return;
        }
        
        this.loadProducts();
    },
    template:
        "<div class='block-body'>" +
        "   <div v-if='!isNew'>" +
        "       <div>{{ title }}</div>" +
        "   </div>" +
        "   <select v-if='isNew' v-on:input='addProduct' class='form-control'>" +
        "       <option value=''>[Не выбрано]</option>" +
        "       <option v-for='product in products' :key='product.id' :value='product.id'>{{ product.title }}</option>" +
        "   </select>" +
        "</div>"
});