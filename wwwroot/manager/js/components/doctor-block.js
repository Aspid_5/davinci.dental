Vue.component("doctor-block", {
    props: ["uid", "model"],
    data: function () {
        return {
            name: '',
            doctors: [],
            isNew: true
        };
    },
    methods: {
        loadDoctor: function (id) {
            let self = this;
            
            fetch("/manager/api/doctor/" + id)
                .then(function (response) { return response.json(); })
                .then(function (result) {
                    self.name = result.name;
                    self.isNew = false;
                });
        },
        loadDoctors: function () {
            let self = this;

            fetch( '/manager/api/doctor/list')
                .then(function (response) {
                    return response.json();
                })
                .then(function (result) {
                    self.doctors = result;
                })
                .catch(function (error) { console.log("error:", error ); });
        },
        addDoctor: function (event) {
            let id = event.target.value;
            
            if (id.length > 0) {
                this.model.doctorId.value = id;
                this.loadDoctor(id);
            }
        }
    },
    created: function () {
        if (this.model.doctorId.value) {
            this.loadDoctor(this.model.doctorId.value);
            return;
        }
        
        this.loadDoctors();
    },
    template:
        "<div class='block-body'>" +
        "   <div v-if='!isNew'>" +
        "       <div>{{ name }}</div>" +
        "   </div>" +
        "   <select v-if='isNew' v-on:input='addDoctor' class='form-control'>" +
        "       <option value=''>[Не выбран]</option>" +
        "       <option v-for='doctor in doctors' :key='doctor.id' :value='doctor.id'>{{ doctor.name }}</option>" +
        "   </select>" +
        "</div>"
});