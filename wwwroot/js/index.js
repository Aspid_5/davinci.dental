$(document).ready(function () {
    (function BackToTop() {
        const button = $('#back-to-top');

        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                button.addClass('show');
            } else {
                button.removeClass('show');
            }
        });

        button.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, '300');
        });
    })();
    
    (function MobileMenu() {
        const button = $('#mobile-menu-toggle > input[type=checkbox]');
        const rootNode = $('.mobile-menu');

        const visibleClass = 'mobile-menu__expand-menu_visible';
        const expandMenuClass = 'mobile-menu__expand-menu';
        
        if (button.prop('checked')) {
            rootNode.toggleClass('mobile-menu_active', true);
        }
        
        const activeLink = rootNode.find('.mobile-menu__link_active');
        if (activeLink.length === 1) {
            activeLink.parents('.' + expandMenuClass).addClass(visibleClass);
        }
        
        button.on('click', function () {
            rootNode.toggleClass('mobile-menu_active');
        });

        rootNode.find('.show-expand').on('click', function () {
            $(this).next().addClass(visibleClass);
        });

        rootNode.find('.hide-expand').on('click', function () {
            $(this).closest('.mobile-menu__expand-menu').removeClass(visibleClass)
        });
    })();

    (function Modal() {
        const options = {
            animatedIn: 'bounceInUp',
            animatedOut: 'bounceOutDown',
            color: '#F5F6FA',
            beforeOpen: function () {
                $('#modal-contact').addClass('modal_initialized');
            }
        };

        $('#show-modal-contact').animatedModal(options);
        
        const $form = $('#modal-contact form');

        function formHandler(token) {
            let $submitButton = $form.find('.btn-submit');
            let $closeButton = $form.closest('.modal').find('.modal__close-action');

            $submitButton.prop('disabled', true);

            $form
                .find('#recaptcha-token')
                .val(token);

            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: $form.serialize()
            }).done(function () {
                $submitButton.prop('disabled', false);
                $closeButton.click();
            }).fail(function () {
                $submitButton.prop('disabled', false);
                $closeButton.click();
            });
        }
        
        $form.submit(function (event) {
            event.preventDefault();
            
            grecaptcha.ready(function () {
                grecaptcha.execute('6LdSWv8UAAAAACBEVnSGHAlGJtOoONL5yvUaxCUU', {action: 'submit'}).then(function (token) {
                    formHandler(token);
                });
            });
        });
    })();
});