$(document).ready(function () {
    (function ServiceMenu() {
        const $menu = $('#service-menu');
        const $parentLinks = $('.service-menu > li > .service-menu__link');
        const classes = 'service-menu__submenu_active animated slideInLeft faster';

        function showSubmenu($parentLink) {
            $menu.find('.service-menu__submenu').removeClass(classes);
            $parentLink.next('.service-menu__submenu').addClass(classes);
        }

        $parentLinks.on('click', function () {
            showSubmenu($(this));
        });

        let $link = $menu.find('.service-menu__link_active')
            .closest('.service-menu__submenu')
            .prev('.service-menu__link');

        if ($link.length !== 0) {
            $link.next('.service-menu__submenu').addClass('service-menu__submenu_active');
        }
    })();
});