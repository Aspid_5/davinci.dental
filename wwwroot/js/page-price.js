$(document).ready(function () {
    (function SlideCategory() {
        $('.category-parent').click(function () {
            $(this).next().slideToggle();
            $(this).toggleClass('expand');
        });

        $('.category-child').click(function () {
            $(this).next().slideToggle();
            $(this).toggleClass('expand');
        });
    })();
});