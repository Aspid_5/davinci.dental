lentadiv = document.getElementById('med-lenta');

if (typeof lentadiv.dataset.loading === 'undefined') {
    lentadiv.dataset.loading = 1; // Ставим, что уже грузим контент
    
    top_position = lentadiv.offsetTop;

    window.onscroll = function() {
        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        var scrollBottom = scrollTop + window.innerHeight;

        sendCurrentTopScrollToIframe(scrollTop, scrollBottom);
    }
}

//Получение сообщений из IFrame
if (window.addEventListener) {
    window.addEventListener("message", onMessageLenta, false);
}
else if (window.attachEvent) {
    window.attachEvent("onmessage", onMessageLenta, false);
}

function onMessageLenta(event) {
    // Check sender origin to be trusted
    //if (event.origin !== "https://like.doctor" && event.origin !== "http://med" ) return;
    var data = event.data;
    if (typeof(window[data.func]) == "function") {
        window[data.func].call(null, data.parameter);
    }
}

// Получение от Спасибо.Доктор высоты окна ленты
function medWidgetHeightLenta(height) {
    lentadiv = document.getElementById('med-lenta-iframe');
    lentadiv.height = height;
}

function sendCurrentTopScrollToIframe(scrollTop, scrollBottom) {
    //Текущее положение фрейма (нужно вычесть его из параметров)
    lentadiv = document.getElementById('med-lenta');
    widget_top_position = lentadiv.offsetTop;

    //Отправляем данные о просмотрах
    document.getElementById('med-lenta-iframe').contentWindow.postMessage({
        'func': 'sendCurrentTopScrollToIframe',
        'parameter': {'top':scrollTop-widget_top_position, 'bottom':scrollBottom-widget_top_position, 'need_post_views_check':1}
    }, "*");
}